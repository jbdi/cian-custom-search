# coding: utf-8
from __future__ import unicode_literals
import json

from flask import (Flask, g, jsonify, request,
                   Response, render_template, url_for)
from elasticsearch import Elasticsearch


index_name = 'cian'
doc_type='advert'
es = Elasticsearch()
es.indices.create(index=index_name, ignore=400)


app = Flask(__name__)
app.config.from_object(__name__)
app.config.from_envvar('FLASKR_SETTINGS', silent=True)
app.config['JSON_AS_ASCII'] = False


@app.route('/')
def index():
    current_page = int(request.args.get('page', 1))
    es_size = 10
    es_from = current_page * es_size - es_size
    # deposit
    # agent_fee
    # client_fee
    # without_client_fee
    # res['hits']['hits'][0]['_source']['description']
    items = es.search(index=index_name, doc_type=doc_type,
                      from_=es_from, size=es_size,
                      q='deposit:<=20000 AND agent_fee:<=70 AND lease_term:2')
    pages_num = items['hits']['total'] / es_size
    pages = range(1, pages_num + 1 + 1)
    return render_template('index.html',
                           items=items['hits']['hits'],
                           pages=pages,
                           total=items['hits']['total'],
                           current_page=current_page)


