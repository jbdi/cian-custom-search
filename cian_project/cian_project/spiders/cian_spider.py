# coding: utf-8
import json

from elasticsearch import Elasticsearch
import scrapy


index_name = 'cian'
doc_type='advert'
es = Elasticsearch()
es.indices.delete(index=index_name, ignore=400)
es.indices.create(index=index_name, ignore=400)


payload = {
    "_type":"flatrent",
    "room":{"type":"terms","value":[1]},
    "for_day":{"type":"term","value":"!1"},
    "price":{"type":"range","value":{"gte":12000,"lte":25000}},
    "engine_version":{"type":"term","value":2},
    "currency":{"type":"term","value":2},
    "region":{"type":"terms","value":[2]},
    "page":{"type":"term","value":1}
}
url = 'https://spb.cian.ru/cian-api/site/v1/offers/search/'


class CianSpider(scrapy.Spider):
    name = "cian"

    def start_requests(self):
        yield scrapy.Request(url, self.parse, method="POST",
                             body=json.dumps(payload))

    def parse(self, response):
        json_response = json.loads(response.body)
        docs = json_response['data']['offersSerialized']
        payload['page']['value'] = payload['page']['value'] + 1

        for item_id, doc in docs.items():
            res = es.index(index=index_name, doc_type=doc_type, id=item_id, body=doc)
        yield scrapy.Request(url, self.parse, method="POST",
                             body=json.dumps(payload))
        yield docs
